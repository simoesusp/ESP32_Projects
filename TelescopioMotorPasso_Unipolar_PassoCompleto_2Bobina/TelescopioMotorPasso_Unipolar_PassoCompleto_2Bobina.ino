// Controle de Motor de Passo Unipolar com PASSO COMPLETO e 2 Bobinas.
// Para o Unipolar, tem que ligar o comum das duas bobinas no GND e e' a Ponte H que conecta ao VCC
// Testamos com o Unipolar 28BYJ48 de 5V 
// Unipolar 28BYJ48 de 5V Alimentado com 7V (pois a PonteH so' ligava com 7v ou mais...) gasta 420mA
// Bobinas do 28BYJ48: Comun (Vermelho), A1 (Rosa), B1 (Laranja), A2 (Azul), B2 (Amarelo)

// Unipolar EM 284 stp-42d221 com 7V gastava 330mA
// Bobinas do EM 284: Comun, A1, B1, A2, B2(vermelho)  Nessa ordem, sendo qu eo comun NAO e' o Vermelho e sim o do outro lado!!


#define LED_BUILTIN 2
#include <ArduinoOTA.h>   // Library to allow programing via WIFI (Choose ip port in Arduino IDE
//#include <WiFi.h>        // Include the Wi-Fi library
//#include <WebServer.h>
#include <analogWrite.h>  // Aparently, you can analogRead, but not analogWrite without a library with ESP32 !!!

#define m1PWM 23    // Monster P5
#define m2PWM 22    // Monster P6

// Atenção: Monster Moto Shield ligada em 6v ou mais :: Tem   que ligar o 3.3V no +5V senao ela nao Enebla
#define INA1 21   //Motor1, entrada A  Monster P7
#define INA2 19   //Motor2, entrada A  Monster P4
#define INB1 18   //Motor1, entrada B  Monster P8
#define INB2 5    //Motor2, entrada B  Monster P9


int b1, b2;
int dir1 = 0;
int dir2 = 0;
int speed1 = 255;
int speed2 = 255; 
/*
const char *ssid = "ESP32 Access Point"; // The name of the Wi-Fi network that will be created
const char *password = "your-password";   // The password required to connect to it, leave blank for an open

WebServer server(80);

void handleRoot() {
  server.send(200, "text/plain", "XUPA FEDERAL!");
}

void handleNotFound() {
  server.send(404, "text/plain", "Banana");
}
*/
void setup() {  
  //LED
  pinMode(LED_BUILTIN, OUTPUT);

  //PWM
  pinMode(m1PWM, OUTPUT);     // Initialize the Motor PWM pin as an output
  pinMode(m2PWM, OUTPUT);
  pinMode(INA1, OUTPUT);
  pinMode(INB1, OUTPUT);
  pinMode(INA2, OUTPUT);
  pinMode(INB2, OUTPUT);
  
  //analogWriteFrequency(200);  

  digitalWrite(INA1, 0); // Frente
  digitalWrite(INB1, 1);
  digitalWrite(INA2, 0);  // Frente
  digitalWrite(INB2, 1);

  digitalWrite(m1PWM, 1);  // Sepeed = 0
  digitalWrite(m2PWM, 1);

  // Serial Begin
  Serial.begin(115200);
  Serial.println("Booting");

/*
  // WIFI Server AP (This Create a WIFI Server in ESP32 with IP: 192.168.4.1  by default) 
  WiFi.softAP(ssid, password);             // Start the access point
  Serial.print("Access Point \"");
  Serial.print(ssid);
  Serial.println("\" started");

  Serial.print("IP address:\t");
  Serial.println(WiFi.softAPIP());         // Send the IP address of the ESP32 to the computer

  server.on("/", handleRoot);  
    
  server.onNotFound(handleNotFound);

  server.on("/ligaled", []() {
    server.send(200, "text/plain", "ligou");
    digitalWrite(LED_BUILTIN, HIGH);
  });

  server.on("/desligaled", []() {
    server.send(200, "text/plain", "apagou");
    digitalWrite(LED_BUILTIN, LOW);
  });

  server.begin();
  Serial.println("HTTP server started");
  //END OF WIFI Server AP


  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { 
      type = "filesystem";
    }

    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");

  // END OF OTA SETUP
  */
}

void loop() {
//  ArduinoOTA.handle();
//  server.handleClient();

      for(int i = 0; i < 4; i++) {
        switch(i) {
          case 0:
              b1=2;
              b2=2;
          break;
          case 1:
              b1=1;
              b2=2;
          break;
          case 2:
              b1=1;
              b2=1;
          break;
          case 3:
              b1=2;
              b2=1;
          break;
        }

        switch(b1) {
          case 0:
              digitalWrite(INA1, 0);
              digitalWrite(INB1, 0);
          break;
          case 1:
              digitalWrite(INA1, 1);
              digitalWrite(INB1, 0);
          break;
          case 2:
              digitalWrite(INA1, 0);
              digitalWrite(INB1, 1);
          break;
        }
      
        switch(b2) {
          case 0:
              digitalWrite(INA2, 0);
              digitalWrite(INB2, 0);
          break;
          case 1:
              digitalWrite(INA2, 1);
              digitalWrite(INB2, 0);
          break;
          case 2:
              digitalWrite(INA2, 0);
              digitalWrite(INB2, 1);
          break;
        }
        
        delayMicroseconds(1500);
    }
   
   //  Serial.println("----------------------");
}
